﻿using System;

namespace NameInformation
{   //Program that greets the user and asks about the user's name
    //Then gives the user some information regarding the user's name, based on what the input was
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Greetings, tell me your name");
            string name = Console.ReadLine();
            int nameLength = name.Length;
            var firstLetter = name[0];
            Console.WriteLine("Your name is " + name + " it has " + nameLength + " characters and starts with letter " + firstLetter);
        }
    }
}
